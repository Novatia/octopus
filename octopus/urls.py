from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'octopus.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^crawler/', include('crawler.urls')),
    url(r'^mailing/', include('mailing_list.urls')),
    url(r'^lc_api/', include('lc_api.urls')),
    url(r'^alert/', include('alert.urls')),
    url(r'^', include('website.urls')),
    url(r'^page/', include('django.contrib.flatpages.urls')),

)
