"""
Django settings for octopus project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

import socket
import os


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '-ju885h_jkwf_pe=b%u*tqd77t953p9n!77oa1sm%u%z#qc-js'

# SECURITY WARNING: don't run with debug turned on in production!

if socket.gethostname() == 'JobsMaster3':
    DEBUG = True
    TEMPLATE_DEBUG = True
    ALLOWED_HOSTS = ['jobsmaster.com.ng', 'www.jobsmaster.com.ng', '*']

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'jobsmaster',
            'USER': 'root',
            'PASSWORD': 'Jack2211989_',
            'HOST': 'localhost',   # Or an IP Address that your DB is hosted on
            'PORT': '3306',
        }
    }

    # HAYSTACK_CONNECTIONS = {
    #         'default': {
    #         'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
    #         'PATH': '/var/www/projects/whoosh_index',
    #     },
    # }

    HAYSTACK_CONNECTIONS = {
        'default': {
            'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
            'URL': 'http://127.0.0.1:9200',
            'INDEX_NAME': 'haystack',
            'TIMEOUT': 60 * 999,
        },
    }

    TEMPLATE_CONTEXT_PROCESSORS = (
        'django.contrib.auth.context_processors.auth',
        'django.core.context_processors.request',
    )

    # HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'


else:
    DEBUG = True
    TEMPLATE_DEBUG = True
    ALLOWED_HOSTS = []

    #mysql user:root and password: Jack2211989_ databasename: jobsmaster

    # Database
    # https://docs.djangoproject.com/en/1.6/ref/settings/#databases

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }

    # whoosh configuration on haystack

    HAYSTACK_CONNECTIONS = {
            'default': {
            'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
            'PATH': os.path.join(os.path.dirname(__file__), 'whoosh_index'),
        },
    }




# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'haystack',
    'crawler',
    'jobs',
    'mailing_list',
    'website',
    'statistics',
    'lc_api',
    'south',
    'alert',

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'octopus.urls'

WSGI_APPLICATION = 'octopus.wsgi.application'





# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = '/var/www/projects/jobsmaster-static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)


SITE_ID = 1

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

FACEBOOK_APP_ID = "1508320122751157"

FACEBOOK_APP_SECRET = "59e5b23109b262467ca1bc6e4969787e"


