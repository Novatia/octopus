from django.shortcuts import render

# Create your views here.

def dashboard_view(request, template_name="dashboard/index.html", page=""):

    context = {
        'page': page,
        'user': request.user,
    }

    return render(request, template_name, context)
