from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
    role_choices = (
        ('super admin', 'Super Admin'),
        ('admin', 'Admin')
    )
    user = models.OneToOneField(User)
    role = models.CharField(max_length=100, choices=role_choices)
