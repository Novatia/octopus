from django.shortcuts import render_to_response, redirect, render
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import ObjectDoesNotExist
from django.template import *
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from ipware.ip import get_ip

from haystack.query import SearchQuerySet
from statistics.models import SearchQuery_, SearchHistory

from datetime import datetime, timedelta

from jobs.models import Company, Category, State, Job

# Create your views here.
theme_path ='website/website-v2/'

title = 'Jobs Search | One search. All Jobs in Nigeria.'
description = ' jobs in Nigeria from other companies websites, jobs boards and newspapers. One search. All jobs in Nigeria. JobsMaster.com.ng.'
total_num_of_results = 10
rss_template = 'website/switch/results.xml'
json_template = 'website/switch/results.json'
language = 'en'
_copyright = 'Copyright 2014 JobsMaster.com.ng'
website_url = 'http://jobsmaster.com.ng'



def index_view(request, template_name=theme_path+'index.html', page_name='home'):
    total_jobs = SearchQuerySet().filter(date__gte=datetime.now() - timedelta(days=60)).count()
    context = {'total': total_jobs,
                'page': page_name,
                'title': title,
                'description': str(total_jobs) + description,
                'categories': Category.objects.all()[:30],
                }




    return render_to_response(template_name, context)

def search_view(request, template_name=theme_path+'search-results.html', page_name='search results'):
    query = request.GET.get('q', '')
    location = request.GET.get('l', '')
    job_type = request.GET.get('jt', '')
    how_many_days = int(request.GET.get('a', 60))
    cat = request.GET.get('c', '')



    SearchQuery_.register(query, location)
    SearchHistory.add_new_query(get_ip(request), query)

    results = SearchQuerySet().all().exclude(title__exact='').order_by('-date')

    if query:
        results = results.filter(text=query)

    if location:
        results = results.filter(location__in=[location, 'nigeria'])

    if how_many_days:
        results = results.filter(date__gte=datetime.now() - timedelta(days=how_many_days))

    if cat:
        results = results.filter(tags=cat)

    if job_type:
        if job_type == 'ft':
            results = results.filter(type__in=['fulltime', 'full-time', 'full time'])
        if job_type == 'pt':
            results = results.filter(type__in=['part-time', 'parttime', 'part time'])
        if job_type == 'p':
            results = results.filter(type__in=['permanent', 'fulltime', 'full-time', 'full time'])
        if job_type == 'i':
            results == results.filter(type__in=['internship', 'it', 'industrial attachment'])

    p = Paginator(results, total_num_of_results)



    page = request.GET.get('page', 1)
    try:
        jobs = p.page(page)
    except PageNotAnInteger:
        jobs = p.page(1)
    except EmptyPage:
        jobs = p.page(p.num_pages)

    _title = title

    if query:
        if 'jobs' in query.lower():
            _title = query.title()
        else:
            _title = query.title() + 'Jobs. '

    if location:
        _title = _title + 'Employment in '+ location.title()

    total_jobs = SearchQuerySet().all().count()

    _description = str(total_jobs) + description

    page_range = p.page_range[int(page):int(page)+10]

    if request.GET.get('switch'):
        switch_context = {
            'title': _title,
            'description': _description,
            'language': language,
            'copyright': _copyright,
            'results': jobs,
        }

        if request.GET.get('switch') == 'rss':
            return render(request, rss_template, switch_context, content_type='text/xml', context_instance=RequestContext(request))
        else:
            return render(request, json_template, switch_context, content_type='application/json', context_instance=RequestContext(request))

    return render_to_response(template_name, {'results': jobs,
                                              'page': page_name,
                                              'q': query,
                                              'l': location,
                                              'a': how_many_days,
                                              'jt': job_type,
                                              'c': cat,
                                              'total': results.count(),
                                              'title': _title,
                                              'description': _description,
                                              'current_page': page,
                                              'page_range': page_range,
                                                })

def company_view(request, template_name=theme_path+"organization.html", page_name="organization", company_name=""):
    company_name = company_name.replace('-', ' ')
    try:
        company = Company.objects.get(name=company_name)
        company_jobs = SearchQuerySet().all().filter(text=company_name)[0:15]
    except:
        company = None
        company_jobs = SearchQuerySet().all()[0:15]
        pass

    _title = company_name + " job vacancies and employment | "+'JobsMaster.com.ng'
    _description = 'Learn more about a career with ' + company_name + ' including all recent jobs, hiring trends, salaries, work environment and more'

    if request.GET.get('switch'):
        switch_context = {
            'title': _title,
            'description': _description,
            'language': language,
            'copyright': _copyright,
            'results': company_jobs,
        }

        if request.GET.get('switch') == 'rss':
            return render(request, rss_template, switch_context, content_type='text/xml', context_instance=RequestContext(request))
        else:
            return render(request, json_template, switch_context, content_type='application/json', context_instance=RequestContext(request))

    context = {
        'title': _title,
        'description': _description,
        'page_name': page_name,
        'company': company,
        'company_jobs': company_jobs,
        'company_name': company_name,
    }
    return render_to_response(template_name, context)


def category_view(request, template_name=theme_path+"search-results.html", page_name="category", slug=''):
    list_of_states_in_nigeria = State.objects.all()

    location = None
    if slug:
        seperated_slugs = slug.split('-jobs')
        category_name = seperated_slugs[0]
        if "in" in slug:
            seperated_slugs = slug.split('in-')
            location = seperated_slugs[1]
    else:
        return redirect('home')
        pass

    try:
        category = Category.objects.get(slug=category_name.lower())
    except ObjectDoesNotExist:
        # redirect to search page with q = sldu and l = location
        return redirect('search')
        pass

    # the search here should be text contains
    how_many_days = 60
    results = SearchQuerySet().all().filter(tags__in=category.get_tags_list()).filter(date__gte=datetime.now() - timedelta(days=how_many_days)).order_by('-date')

    if location:
        if location.lower() is not "nigeria":
            try:
                location_obj = State.objects.get(slug=location.lower())
                results = results.filter(location__in=location_obj.cities.split(','))
            except ObjectDoesNotExist:
                pass
    total_jobs = results.count()

    p = Paginator(results, total_num_of_results)



    page = request.GET.get('page', 1)
    try:
        jobs = p.page(page)
    except PageNotAnInteger:
        jobs = p.page(1)
    except EmptyPage:
        jobs = p.page(p.num_pages)
    page_range = p.page_range[int(page):int(page)+10]


    _title = category_name.title().replace("-", " ") + ' jobs in Nigeria | JobsMaster.com.ng'
    _description = str(total_jobs)+' '+category_name.title()+' jobs in Nigeria. Jobsmaster.com.ng. one search. All jobs in Nigeria'

    if request.GET.get('switch'):
        switch_context = {
            'title': _title,
            'description': _description,
            'language': language,
            'copyright': _copyright,
            'results': jobs,
        }

        if request.GET.get('switch') == 'rss':
            return render(request, rss_template, switch_context, content_type='text/xml', context_instance=RequestContext(request))
        else:
            return render(request, json_template, switch_context, content_type='application/json', context_instance=RequestContext(request))


    context = {
        'results': jobs,
        'title': _title,
        'description': _description,
        'page_name': page_name,
        'total': total_jobs,
        'current_page':page,
        'page_range': page_range,
        'q': category_name,
        'l': location,
        'states': list_of_states_in_nigeria,
    }

    return render_to_response(template_name, context)


def companies_view(request, template_name=theme_path+"organizations.html", page_name="companies"):
    list_companies = Company.objects.all()
    query = ''

    if request.GET.get('search'):
        query = request.GET.get('search')
        list_companies = Company.objects.filter(name__contains=query)

    total_companies_hiring_list = 30
    p = Paginator(list_companies, total_companies_hiring_list)



    page = request.GET.get('page', 1)
    try:
        companies = p.page(page)
    except PageNotAnInteger:
        companies = p.page(1)
    except EmptyPage:
        companies = p.page(p.num_pages)
    page_range = p.page_range[int(page):int(page)+10]


    _title = "Companies hiring in Nigeria | JobsMaster.com.ng"
    _description = "Discover companies currency hiring in Nigeria. Good salaries, working conditions, vertical promotions. JobsMaster.com.ng"


    context = {
        'title': _title,
        'description': _description,
        'results': companies,
        'page_range': page_range,
        'current_page': page,
        'page_name': page_name,
        'query': query,
    }

    return render_to_response(template_name, context)


def single_view(request, template_name=theme_path+"single.html", page_name="companies"):
    pk = request.GET.get('pk')

    try:
        job = Job.objects.get(pk=pk)
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('search'))

    if job.title:
        s_title = job.title
    else:
        s_title = job.meta_title

    similar_jobs = SearchQuerySet().all().filter(text=s_title).order_by('-date')[0:5]

    context = {
        'page_name': page_name,
        'title': job.title,
        'description': job.description,
        'job': job,
        'similar_jobs': similar_jobs,
    }
    return render(request, template_name, context)
