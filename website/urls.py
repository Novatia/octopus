from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, RedirectView
from django.conf import settings

urlpatterns = patterns('website.views',
                       url(r'^$', 'index_view', name="home"),
                       url(r'^favicon\.ico$', RedirectView.as_view(url=settings.STATIC_URL + 'favicon.ico')),
                       url(r'^search/$', 'search_view', name='search'),
                       url(r'^job/$', 'single_view', name='job'),
                       url(r'^companies-hiring-in-Nigeria.html/$', 'companies_view', name='organizations'),
                       url(r"^cmp/(?P<company_name>[0-9A-Za-z-_()&><[,'|.//]+)/$", 'company_view', name='organization'),
                       url(r"^(?P<slug>[0-9A-Za-z-_()|&><[,'.//]+).html/$", 'category_view', name='categories'),

                       )