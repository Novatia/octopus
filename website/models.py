from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    roles = (
        ('user', 'user'),
        ('manager', 'manager'),
    )
    user = models.OneToOneField(User)
    role = models.CharField(max_length=20, choices=roles)
