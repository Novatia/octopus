from django.contrib import admin
from models import SearchQuery_, SearchHistory

# Register your models here.
class SearchQueryAdmin(admin.ModelAdmin):
    list_display = ('query', 'location', 'popularity')

class SearchHistoryAdmin(admin.ModelAdmin):
    list_display = ('ip', 'queries')



admin.site.register(SearchQuery_, SearchQueryAdmin)
admin.site.register(SearchHistory, SearchHistoryAdmin)
