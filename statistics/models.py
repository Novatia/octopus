from django.db import models
from django.core.exceptions import ObjectDoesNotExist

# Create your models here.

class SearchQuery(models.Model):
    query = models.CharField(max_length=200, blank=True)
    location = models.CharField(max_length=100, blank=True)
    popularity = models.IntegerField(default=1)

    @classmethod
    def register(cls, query, location):
        try:
            s = cls.objects.get(query=query, location=location)
            s.popularity += 1
            s.save()
        except ObjectDoesNotExist:
            cls.objects.create(query=query, location=location)
            pass

class SearchQuery_(models.Model):
    query = models.CharField(max_length=200, blank=True)
    location = models.CharField(max_length=100, blank=True)
    popularity = models.IntegerField(default=1)

    @classmethod
    def register(cls, query, location):
        try:
            s = cls.objects.get(query=query, location=location)
            s.popularity += 1
            s.save()
        except ObjectDoesNotExist:
            cls.objects.create(query=query, location=location)
            pass


class SearchHistory(models.Model):
    ip = models.IPAddressField()
    queries = models.TextField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    # location = models.CharField(null=True, blank=True)
    # k = models.CharField(max_length=20, null=True, blank=True)

    @classmethod
    def add_new_query(cls, ip, query):
        try:
            obj = cls.objects.get(ip=ip)
        except ObjectDoesNotExist:
            obj = cls()
            obj.ip = ip

        if obj.queries:
            obj.queries = obj.queries + ', ' + query
        else:
            obj.queries = query
        # obj.location = obj.location + ', '+ location

        obj.save()


