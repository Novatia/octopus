from django.db import models
from django.contrib.auth.admin import User
from django.db.models import ObjectDoesNotExist


from jobs.models import State

# Create your models here.


class Alert(models.Model):
    type_choices = (
        ('fulltime', 'Full-Time'),
        ('parttime', 'Part-Time'),
        ('permanent', 'Permanent'),
        ('internship', 'Internship'),
        (' ', ' '),
    )

    status_choices = (
        ('on', 'On'),
        ('off', 'Off'),
    )

    tel = models.CharField(max_length=11, null=True, blank=True)
    email = models.EmailField()
    location = models.ForeignKey(State, null=True, blank=True)
    fullname = models.CharField(max_length=100, blank=True, null=True)
    type = models.CharField(max_length=20, choices=type_choices, default="")
    keyword = models.CharField(max_length=100)
    status = models.CharField(max_length=100, choices=status_choices, default="on")
    created_by = models.ForeignKey(User, null=True, blank=True)
    time = models.DateTimeField(auto_now_add=True)

    @classmethod
    def create_new_alert(cls, email, q):
        try:
            cls.objects.get(email=email, keyword=q)
        except ObjectDoesNotExist:
            a = cls()
            a.keyword = q
            a.email = email
            a.save()



