from django.shortcuts import render
from django.http import HttpResponse

from forms import AlertForm
from models import Alert

# Create your views here.

def alert_view(request, template_name="alert/alert.html", page_name=""):
    title = "Job Search Alerts - JobsMaster.com.ng"
    description = "email and mobile job alert job search JobsMaster.com.ng one search. all jobs in Nigeria."


    form = AlertForm()

    message = ""

    if request.method == "POST":
        form = AlertForm(request.POST)
        if form.is_valid():
            alert = form.save(commit=False)
            if request.user.is_authenticated():
                alert.created_by = request.user

            alert.save()
                # return HttpResponseRedirect(reverse('alert'))
            message = "Successfully created alert"


    context = {
        'form': form,
        'page_name': page_name,
        'title': title,
        'description': description,
        'message': message,
    }
    return render(request, template_name, context)


def daily_alerts(request):

    alerts = Alert.objects.filter(status='on')

    for alert in alerts:
        pass

    return HttpResponse()