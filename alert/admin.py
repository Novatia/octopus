from django.contrib import admin

from models import Alert

# Register your models here.

class AlertAdmin(admin.ModelAdmin):
    list_display = ('fullname', 'tel', 'email', 'location', 'type', 'keyword', 'status')

admin.site.register(Alert, AlertAdmin)
