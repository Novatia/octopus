# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tag'
        db.create_table(u'jobs_tag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('popularity', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'jobs', ['Tag'])

        # Adding model 'Category'
        db.create_table(u'jobs_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=200)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
        ))
        db.send_create_signal(u'jobs', ['Category'])

        # Adding M2M table for field tags on 'Category'
        m2m_table_name = db.shorten_name(u'jobs_category_tags')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('category', models.ForeignKey(orm[u'jobs.category'], null=False)),
            ('tag', models.ForeignKey(orm[u'jobs.tag'], null=False))
        ))
        db.create_unique(m2m_table_name, ['category_id', 'tag_id'])

        # Adding model 'Company'
        db.create_table(u'jobs_company', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'jobs', ['Company'])

        # Adding model 'State'
        db.create_table(u'jobs_state', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('cities', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'jobs', ['State'])

        # Adding model 'Job'
        db.create_table(u'jobs_job', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('meta_title', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('meta_description', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(default='', max_length=20, blank=True)),
            ('company', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['jobs.Company'], null=True)),
            ('landing_page', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('source', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('salary', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('location', self.gf('django.db.models.fields.CharField')(default='', max_length=50, blank=True)),
            ('qualification', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True)),
            ('years_of_qualifications', self.gf('django.db.models.fields.CharField')(default=0.0, max_length=100)),
            ('crawled_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('popularity', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'jobs', ['Job'])

        # Adding M2M table for field tags on 'Job'
        m2m_table_name = db.shorten_name(u'jobs_job_tags')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('job', models.ForeignKey(orm[u'jobs.job'], null=False)),
            ('tag', models.ForeignKey(orm[u'jobs.tag'], null=False))
        ))
        db.create_unique(m2m_table_name, ['job_id', 'tag_id'])


    def backwards(self, orm):
        # Deleting model 'Tag'
        db.delete_table(u'jobs_tag')

        # Deleting model 'Category'
        db.delete_table(u'jobs_category')

        # Removing M2M table for field tags on 'Category'
        db.delete_table(db.shorten_name(u'jobs_category_tags'))

        # Deleting model 'Company'
        db.delete_table(u'jobs_company')

        # Deleting model 'State'
        db.delete_table(u'jobs_state')

        # Deleting model 'Job'
        db.delete_table(u'jobs_job')

        # Removing M2M table for field tags on 'Job'
        db.delete_table(db.shorten_name(u'jobs_job_tags'))


    models = {
        u'jobs.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['jobs.Tag']", 'symmetrical': 'False'})
        },
        u'jobs.company': {
            'Meta': {'object_name': 'Company'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'jobs.job': {
            'Meta': {'ordering': "['-crawled_date']", 'object_name': 'Job'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['jobs.Company']", 'null': 'True'}),
            'content': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'crawled_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'landing_page': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'location': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '50', 'blank': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'meta_title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'popularity': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'qualification': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'salary': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'source': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['jobs.Tag']", 'null': 'True', 'symmetrical': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'years_of_qualifications': ('django.db.models.fields.CharField', [], {'default': '0.0', 'max_length': '100'})
        },
        u'jobs.state': {
            'Meta': {'object_name': 'State'},
            'cities': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'jobs.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'popularity': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['jobs']