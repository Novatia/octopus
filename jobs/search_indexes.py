from haystack import indexes
from jobs.models import Job

import datetime
class JobIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    meta_title = indexes.CharField(model_attr='meta_title')
    content = indexes.CharField(model_attr='content')
    summary = indexes.CharField(model_attr='meta_description')
    tags = indexes.MultiValueField()
    url = indexes.CharField(model_attr='landing_page')
    location = indexes.CharField(model_attr='location')
    type = indexes.CharField(model_attr='type')
    company = indexes.CharField(model_attr='company', null=True)
    qualifications = indexes.CharField(model_attr='qualification')
    source = indexes.CharField(model_attr='source')
    date = indexes.DateTimeField(model_attr='crawled_date')


    def get_model(self):
        return Job

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(crawled_date__lte=datetime.datetime.now())

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]