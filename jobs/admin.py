from django.contrib import admin
from models import Job, Tag, Category, State, Company

# Register your models here.
class JobAdmin(admin.ModelAdmin):
    list_display = ('title', 'source', 'landing_page')

class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'logo', 'about', 'state', 'website', 'facebook', 'twitter', 'googleplus', 'address', 'industry', 'employees', 'stock_symbol')

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

class StateAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(Job, JobAdmin)
admin.site.register(Tag)
admin.site.register(Category, CategoryAdmin)
admin.site.register(State, StateAdmin)
admin.site.register(Company, CompanyAdmin)
