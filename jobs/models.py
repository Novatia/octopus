from django.db import models
from django.core.exceptions import ObjectDoesNotExist

# Create your models here.

class Tag(models.Model):
    name = models.CharField(max_length=200)
    popularity = models.IntegerField(default=0)

    @classmethod
    def get_tag_with_name(cls, name):
        try:
            t = cls.objects.get(name=name)
        except Exception:
            t = cls.objects.create(name=name, popularity=0)

        return t


    def __unicode__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField()
    tags = models.ManyToManyField(Tag)

    def __unicode__(self):
        return self.name

    def get_tags_list(self):
        return [tag.name for tag in self.tags.all()]



class State(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField()
    cities = models.TextField()

    def __unicode__(self):
        return self.name

    # @classmethod
    # def get_list_of_states_in_nigeria_by_slug(cls):
    #     return [state.slug for state in cls.objects.all()]

class Company(models.Model):
    name = models.CharField(max_length=200)
    logo = models.ImageField(upload_to='/static/images/', null=True, blank=True)
    about = models.TextField(null=True, blank=True)
    state = models.ForeignKey(State, null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    facebook = models.URLField(null=True, blank=True)
    twitter = models.URLField(null=True, blank=True)
    googleplus = models.URLField(null=True, blank=True)
    address = models.CharField(max_length=200, null=True, blank=True)
    industry = models.ForeignKey(Category, null=True, blank=True)
    employees = models.CharField(max_length=200, null=True, blank=True)
    stock_symbol = models.CharField(max_length=200, null=True, blank=True)


    @classmethod
    def get_company_with_name(cls, name):
        try:
            c = cls.objects.get(name=name)
            return c
        except ObjectDoesNotExist:
            c = cls.objects.create(name=name)
            return c

    def __unicode__(self):
        return self.name


class Job(models.Model):
    meta_title = models.CharField(max_length=200, blank=True, default='')
    meta_description = models.CharField(max_length=200, blank=True, default='')

    title = models.CharField(max_length=200, blank=True, default='')
    description = models.TextField(blank=True, default='')
    content = models.TextField(blank=True, default='')
    type = models.CharField(max_length=20, blank=True, default='')
    company = models.ForeignKey(Company, null=True)
    landing_page = models.URLField()
    source = models.CharField(max_length=200, blank=True, default='')

    salary = models.FloatField(default=0.0)

    location = models.CharField(max_length=50, blank=True, default='')

    qualification = models.CharField(max_length=100, blank=True, default='')

    years_of_qualifications = models.CharField(max_length=100, default=0.0)

    crawled_date = models.DateTimeField(auto_now_add=True)

    popularity = models.IntegerField(default=0)

    tags = models.ManyToManyField(Tag, null=True)


    def __unicode__(self):
        return self.title


    class Meta:
        ordering = ['-crawled_date']


