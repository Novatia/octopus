from django.db import models

# Create your models here.

class Feedback(models.Model):
    status_choices = (
        ('viewed', 'Viewed'),
        ('new', 'New')
    )
    subject = models.CharField(max_length=50)
    summary = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=10, choices=status_choices)


class FundRequest(models.Model):
    fund_request_status = (
        ('processing', 'Processing'),
        ('declined', 'Declined'),
        ('processed', 'Processed'),
        ('pending', 'Pending'),
    )
    amount = models.DecimalField(decimal_places=2, max_digits=65)
    acc_id = models.CharField(max_length=100)
    acc_name = models.CharField(max_length=100)
    status = models.CharField(max_length=100, choices=fund_request_status)
    time = models.DateTimeField(auto_now_add=True)


class NewsUpdate(models.Model):
    title = models.CharField(max_length=200)
    url = models.URLField()
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date']

