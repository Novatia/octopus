from django.shortcuts import render, HttpResponse
from django.views.decorators.http import require_POST, require_GET
from models import Feedback, FundRequest, NewsUpdate

from django.views.decorators.csrf import csrf_exempt
# Create your views here.

@csrf_exempt
@require_POST
def feedback_view(request):
    subject = request.POST.get('subject')
    summary = request.POST.get('summary')

    if subject and summary:
        feedback = Feedback()
        feedback.subject = subject
        feedback.summary = summary
        feedback.status = 'new'
        feedback.save()

        return HttpResponse('successful')
    else:
        return HttpResponse('failed')

@csrf_exempt
@require_POST
def fund_request_view(request):
    acc_id = request.POST.get('accid')
    acc_name = request.POST.get('accname')
    amount = request.POST.get('amount')

    if acc_id and acc_name and amount:
        f = FundRequest()
        f.acc_name = acc_name
        f.acc_id = acc_id
        f.amount = amount
        f.status = 'pending'
        f.save()

        return HttpResponse('successful')
    else:
        return HttpResponse('failed')

@require_GET
def display_news_and_updates(request, template_name='lc/rss.xml'):
    output = request.GET.get('output')

    if output == 'rss':
        updates = NewsUpdate.objects.all()[0:10]

        context = {
            'title': '',
            'description': '',
            'language': '',
            'results': updates,
            'copyright': ''
        }

        return render(request, template_name, context, content_type='text/xml')


