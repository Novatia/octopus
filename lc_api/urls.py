from django.conf.urls import patterns, include, url


urlpatterns = patterns('lc_api.views',
   url('^feedback/$', 'feedback_view', name='feedback'),
   url('^news/$', 'display_news_and_updates', name='news'),
   url('^fund/$', 'fund_request_view', name='funds'),
)
