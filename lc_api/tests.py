from django.test import TestCase, Client
from api.models import Feedback, FundRequest, NewsUpdate

# Create your tests here.


class FeedbackTestCase(TestCase):
    def setUp(self):
        pass

    def test_feedback_submitted_successfully(self):
        c = Client()

        response = c.post('/api/feedback/', {'subject': 'my feedback', 'summary': 'Hey there do you have a pen'})

        self.assertEqual(response.content, 'successful')

    def test_feedback_stored(self):
        c = Client()

        c.post('/api/feedback/', {'subject': 'my feedback', 'summary': 'Hey there do you have a pen'})

        self.assertNotEqual(Feedback.objects.all().count(), 0)

    def test_feedback_failed_without_subject(self):
        c = Client()

        response = c.post('/api/feedback/', {'subject': '', 'summary': 'Hey there do you have a pen'})

        self.assertEqual(response.content, 'failed')

    def test_feedback_failed_without_summary(self):
        c = Client()

        response = c.post('/api/feedback/', {'subject': 'Gey', 'summary': ''})

        self.assertEqual(response.content, 'failed')


class FundRequestCase(TestCase):
    def setUp(self):
        pass

    def test_fund_request_submitted(self):
        c = Client()

        response = c.post('/api/fund/', {'accid': '5343h', 'accname': 'OLA3107', 'amount': '100'})

        self.assertEqual(response.content, 'successful')

        pass

    def test_fund_request_stored(self):
        c = Client()

        response = c.post('/api/fund/', {'accid': '5343h', 'accname': 'OLA3107', 'amount': '100'})

        self.assertEqual(FundRequest.objects.count(), 1)

        pass

    def test_fund_request_failed_without_accid(self):
        c = Client()

        response = c.post('/api/fund/', {'accid': '', 'accname': 'OLA3107', 'amount': '100'})

        self.assertEqual(response.content, 'failed')

        pass

    def test_fund_request_failed_without_accname(self):
        c = Client()

        response = c.post('/api/fund/', {'accid': '5343h', 'accname': '', 'amount': '100'})

        self.assertEqual(response.content, 'failed')

        pass

    def test_fund_request_failed_wihtout_amount(self):
        c = Client()

        response = c.post('/api/fund/', {'accid': '5343h', 'accname': 'OLA3107', 'amount': ''})

        self.assertEqual(response.content, 'failed')

        pass

