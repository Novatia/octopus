from django.contrib import admin

from models import Feedback, FundRequest, NewsUpdate

# Register your models here.

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('subject', 'summary', 'date', 'status')

class FundRequestAdmin(admin.ModelAdmin):
    list_display = ('acc_id', 'acc_name', 'amount', 'time', 'status')

class NewsAndUpdateAdmin(admin.ModelAdmin):
    list_display = ('title', 'url', 'date')


admin.site.register(Feedback, FeedbackAdmin)
admin.site.register(FundRequest, FundRequestAdmin)
admin.site.register(NewsUpdate, NewsAndUpdateAdmin)
