from django.shortcuts import render
from django.http.response import HttpResponse

from models import Subscribers
from strings import welcome_mail_subject, welcome_mail_body, daily_mail_body, html_newsletter, html_newsletter_result
from haystack.query import SearchQuerySet

from jobs.models import Job

from alert.models import Alert

import sendgrid
from datetime import datetime, timedelta
# Create your views here.

sendgrid_username = 'darilldrems'
sendgrid_password = 'Jack2211989_'


def subscribe(request):
    email = request.GET.get('email', '')
    first_name = request.GET.get('firstname', '')
    last_name = request.GET.get('lastname', '')
    q = request.GET.get('q', '')

    Alert.create_new_alert(email, q)



    if not email:
        return HttpResponse('false')




    if not Subscribers.email_in_db(email):

        Subscribers.objects.create(email=email, first_name=first_name, last_name=last_name, status='subscribed')
        sg = sendgrid.SendGridClient(sendgrid_username, sendgrid_password)

        message = sendgrid.Mail()
        message.add_to(email)
        message.set_subject(welcome_mail_subject)
        edited_mail = welcome_mail_body.replace('[email]', email)
        message.set_html(edited_mail)
        message.set_from('noreply@jobsmaster.com.ng')
        sg.send(message)
        return HttpResponse('true')

    return HttpResponse('false')

def unsubscribe(request):
    email = request.GET.get('email')

    if Subscribers.email_in_db(email):
        s = Subscribers.objects.get(email=email)
        s.status = 'unsubscribed'
        s.save()

        return HttpResponse('true')

    return HttpResponse('false')

def send_daily_emails(request):
    subscribers = Subscribers.objects.filter(status="subscribed").all()
    total_jobs = SearchQuerySet().all().count()
    total_recent_jobs = Job.objects.filter(crawled_date__gte=datetime.now() - timedelta(hours=10)).exclude(title=" ")[0:20]

    total = Job.objects.filter(crawled_date__gte=datetime.now() - timedelta(hours=10)).exclude(title=" ").count()

    job_results = ""
    html_email = html_newsletter

    newsleter_row = html_newsletter_result

    for job in total_recent_jobs:
        job_result = newsleter_row
        company = " "
        location = " "
        if not job.title == "":
            title = job.title.title()
        else:
            title = job.meta_title.title()
        job_result = job_result.replace('[title]', title)
        job_result = job_result.replace('[href]', 'http://jobsmaster.com.ng/job/?pk='+str(job.pk))
        if job.company:
            company = job.company.name.title()

        if job.location:
            location = job.location.title()
        job_result = job_result.replace('[company]', company)
        job_result = job_result.replace('[location]', location)

        job_results += job_result

    html_email = html_email.replace('[jobresults]', job_results)
    html_email = html_email.replace('[total]', str(20))
    html_email = html_email.replace('[total_count]', str(total_jobs))

    for subscriber in subscribers:
        # newsletter = daily_mail_body
        # newsletter = newsletter.replace('[email]', subscriber.email)
        # newsletter = newsletter.replace('[total]', str(total_jobs))
        # newsletter = newsletter.replace('[recent_total]', str(total_recent_jobs))

        newsletter = html_email.replace('[email]', subscriber.email)

        sg = sendgrid.SendGridClient(sendgrid_username, sendgrid_password)
        message = sendgrid.Mail()
        message.add_to(subscriber.email)
        message.set_subject('Jobs Nigeria Jobs Alert')
        message.set_html(newsletter)
        message.set_from('JobsMaster Alert<noreply@jobsmaster.com.ng>')
        sg.send(message)

    return HttpResponse('')
    pass

def send_alert_emails(request):
    alerts = Alert.objects.all()

    for alert in alerts:
        keywords = alert.keyword.split(',')

        for keyword in keywords:
            jobs = SearchQuerySet().all().filter(text=keyword).filter(date__gte=datetime.now() - timedelta(days=1)).order_by('-date')[:20]
            total = 20
            job_result = html_newsletter_result
            job_results = ""
            html_email = html_newsletter

            if job:
                for job in jobs:
                    if job.title == " ":
                        title = job.meta_title
                    else:
                        title = job.title

                    if job.company:
                        company = job.company
                    else:
                        company = ""

                    if job.location:
                        location = job.location
                    else:
                        location = ""

                    job_result = job_result.replace('[title]', title)
                    job_result = job_result.replace('[href]', 'http://jobsmaster.com.ng/job/?pk='+str(job.pk))
                    job_result = job_result.replace('[company]', company)
                    job_result = job_result.replace('[location]', location)
                    job_results += job_result
                html_email = html_email.replace('[jobresults]', job_results)
                html_email = html_email.replace('[total]', str(total))
                html_email = html_email.replace('[total_count]', str(SearchQuerySet().all().filter(text=keyword).count()))
                html_email = html_email.replace('[email]', alert.email)

                sg = sendgrid.SendGridClient(sendgrid_username, sendgrid_password)
                message = sendgrid.Mail()
                message.add_to(alert.email)
                message.set_subject(alert.keyword.title()+' Jobs Nigeria Jobs')
                message.set_html(html_email)
                message.set_from('JobsMaster Alert<noreply@jobsmaster.com.ng>')
                sg.send(message)
    return HttpResponse()
