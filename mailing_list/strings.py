contact_us_url = 'http://jobsmaster.com.ng/page/contact/'
website_home_page_url = 'http://jobsmaster.com.ng'
category_page_url = 'http://jobsmaster.com.ng/search/'
unsubscribe_url = 'http://jobsmaster.com.ng/mailing/unsubscribe?email='
keyword = "jobs in Nigeria"
site_ = "jobsmaster.com.ng"
alert_url = "http://jobsmaster.com.ng/alert/email_sms/"
seven_days_url = "http://jobsmaster.com.ng/search/?a=7&l=&q=&jt=&c="
logo_url = "http://jobsmaster.com.ng/static/jobsmaster-logo.png"

welcome_mail_subject = "Welcome to JobsMaster.com.ng"
welcome_mail_body = """
    <h1>Welcome to <a href="{0}">JobsMaster</a> Job Alerts.</h1>

    <p>Thanks for your request on JobsMaster to send New Job Alerts to your email for <a href="{0}">Jobs in Nigeria</a>.</p>

    <p>We will start sending you new matches as soon as we find them or you can see the latest job matching your
    search <a href="{2}">here</a></p>

    <p>If you still require further support please don't hesitate to <a href="{1}">contact us</a></p>

    <a href="{0}">JobsMaster.com.ng</a> <small>one search. all jobs in Nigeria.</small>

    <p>powered by CloudOne Ltd</p>

    <small><a href="{3}[email]"></a></small>
""".format(website_home_page_url, contact_us_url, category_page_url, unsubscribe_url)

daily_mail_body = """
    <h1>[total] Jobs in Nigeria on <a href="{0}">JobsMaster.com.ng<a></h1>

    <p>Hi [email],</p>

    <p><a href="{0}">[recent_total] newly posted jobs<a> for you in Nigeria on JobsMaster.com.ng.</p>

    <p>You asked us to always let you know when there are new jobs in Nigeria. We will be on the look out for you and inform you when there are more jobs.</p>

    <p>Visit <a href="{0}">JobsMaster.com.ng</a> and apply for new jobs posted in Nigeria today.</p>
""".format(category_page_url)


# {0} jobsmaster.com.ng, {1}jobs in nigeria, {3}http://jobsmaster.com.ng, {4}alert url, {5}7 days, {6} all jobs, {7}unsubscribe

html_newsletter = """
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta content="text/html; charsetUTF-8" http-equiv="Content-Type">
        <title>{0} email alert</title>
    </head>
        <body>
            <div id="container" style='font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;font-size:14px;line-height:26px;color:#333333;background-color:white'>
                <div id="logo" style=margin-bottom:20px">
                    <img alt="Logo" height="41" src="{2}">
                    </div>
                    <div id="heading">
                        <font size="+1">{1}</font>
                    </div>
                    <div id="entries_info">Displaying jobs
                        <b>1 - [total]</b> of
                        <b>[total_count]</b> in total
                    </div>
                    <div id="jobresults" style="margin-top:10px;margin-bottom:15px">

                    [jobresults]

                    </div>

                            <div class="search_links" style="font-size:12px">
                                <span style="margin-right:7px">View jobs:</span>
                                <a href="{5}" style="color:#0088cc">for last 7 days</a> -
                                <a href="{6}" style="color:#0088cc">all jobs</a>
                            </div>
                            <ul id="actions" style="margin-top:3em;margin-bottom:1em;padding:5px 0px;margin-left:0px;font-size:12px;line-height:22px;border-top:1px solid #e5e5e5;border-bottom:1px solid #e5e5e5;list-style:none">

                                <li style="padding-left:10px">
                                    <a href="{4}" style="color:#0088cc">Create</a> a new email alert
                                </li>


                                <li style="padding-left:10px">
                                    <a href="{7}[email]" style="color:#0088cc">Cancel</a> this email alert
                                </li>


                            </ul>
                            <br>
                                <font size="-1">
                                    <a href="{3}" style="color:#0088cc"></a>
                                    <span style="color: #6f6f6f">one click - all jobs in Nigeria.</span>
                                    <p>Cloud One Softwares Limited
                                        <span> - </span>
                                        <span>{3}</span>
                                    </p>
                                </font>

                                </div>
                            </body>
                        </html>


""".format(site_, keyword, logo_url, website_home_page_url, alert_url, seven_days_url, category_page_url, unsubscribe_url)

html_newsletter_result = """
<div class="result tap_item first" style="line-height:20px;padding:10px 0;border-bottom:1px solid #e5e5e5;border-top:1px solid #e5e5e5">
                            <a href="[href]" style="color:#0088cc;text-decoration:none;display:block">
                                <span class="job_title" style="font-size:16px;line-height:18px;font-weight:bold;margin-bottom:6px;display:block;margin-top:0px;color:#333">
                                    [title]
                                </span>
                                <div class="comp_loc">
                                    <span class="company" style="font-size:11pt;color:#333333">[company]</span> -
                                    <span class="location" style="color:#666666;font-size:11pt">[location]</span>
                                </div>
                            </a>
                        </div>
"""