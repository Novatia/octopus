from django.contrib import admin
from models import Subscribers

# Register your models here.

class SubscribersAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name')

admin.site.register(Subscribers)
