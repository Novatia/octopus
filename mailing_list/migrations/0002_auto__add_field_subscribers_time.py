# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Subscribers.time'
        db.add_column(u'mailing_list_subscribers', 'time',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.date.today(), blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Subscribers.time'
        db.delete_column(u'mailing_list_subscribers', 'time')


    models = {
        u'mailing_list.subscribers': {
            'Meta': {'object_name': 'Subscribers'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['mailing_list']