from django.conf.urls import patterns, url

urlpatterns = patterns('mailing_list.views',
                       url(r'^subscribe/$', 'subscribe', name='subscribe'),
                       url(r'^unsubscribe/$', 'unsubscribe', name='unsubscribe'),
                       url(r'^send_daily_mail/$', 'send_daily_emails', name='daily_emails'),
                       url(r'^send_alert_emails/$', 'send_alert_emails', name='alert_emails'),
                       )