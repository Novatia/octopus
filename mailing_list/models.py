from django.db import models
from django.core.exceptions import ObjectDoesNotExist

# Create your models here.

class Subscribers(models.Model):
    subscription = (
        ('subscribed', 'Subscribed'),
        ('unsubscribed', 'Unsubscribed')
    )
    email = models.EmailField()
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    status = models.CharField(max_length=20, choices=subscription)
    time = models.DateTimeField(auto_now_add=True)

    @classmethod
    def email_in_db(cls, email):
        try:
            cls.objects.get(email=email)
            return True
        except ObjectDoesNotExist:
            return False

    def __unicode__(self):
        return self.email