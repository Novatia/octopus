from django.test import TestCase
from django.test.client import Client

from models import Subscribers

# Create your tests here.

class SubscribersModelTestCase(TestCase):
    def setUp(self):
        Subscribers.objects.create(email='darilldrems@gmail.com', first_name='Ridwan', last_name='Olalere', status='subscribed')
        pass

    def test_email_in_db_works(self):
        result = Subscribers.email_in_db('darilldrems@gmail.com')
        self.assertTrue(result)

    def test_email_in_db_works_with_invalid_email(self):
        result = Subscribers.email_in_db('darill@gmail.com')
        self.assertFalse(result)


class SubscribersViewTestCase(TestCase):
    def setUp(self):
        Subscribers.objects.create(email='darill@gmail.com', first_name='Ridwan', last_name='Olalere', status='subscribed')
        pass

    def test_subscribe_works(self):
        c = Client()
        response = c.get('/mailing/subscribe/', {'email':'darilldrems@gmail.com', 'firstname':'Ridwan', 'lastname':'Olalere'})

        self.assertTrue(Subscribers.email_in_db('darilldrems@gmail.com'))

    def test_unsubscribe_works(self):
        c = Client()
        response = c.get('/mailing/unsubscribe/', {'email': 'darill@gmail.com'})

        fake = Subscribers.objects.get(email='darill@gmail.com')

        self.assertEqual(fake.status, 'unsubscribed')


