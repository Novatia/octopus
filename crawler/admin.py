from django.contrib import admin
from models import Index, Source

# Register your models here.


admin.site.register(Index)
admin.site.register(Source)
