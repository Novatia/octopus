from django.conf.urls import patterns, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('crawler.views',
    # Examples:
    # url(r'^$', 'octopus.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^index_crawler/$', 'index_crawler', name='crawler'),
    url(r'data_crawler/$', 'data_crawler', name='data'),
)
