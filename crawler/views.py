# from django.shortcuts import render
from lxml import html
import requests
from requests.exceptions import ConnectionError
from urllib2 import URLError
from lxml.etree import XMLSyntaxError
import re
import random

from helpers import get_formated_salary, split_and_get_formatted_tags
from models import Index, Source
from jobs.models import Job, Tag, Company

from django.http.response import HttpResponse
from django.db import OperationalError

from boilerpipe.extract import Extractor

from urllib3.exceptions import LocationParseError


# Create your views here.


def index_crawler(request):


    sources = Source.objects.all()

    for source in sources:
        try:
            start_url = source.start_url

            base_path = source.base_path
            xpath = source.link_xpath


            page = requests.get(start_url)
            tree = html.fromstring(page.text)

            links = tree.xpath(xpath)

            if links:
                if 'http://' not in links[0]:
                    links_with_base = map(lambda x: base_path + x, links)
                else:
                    links_with_base = links

                Index.add_to_index(links_with_base, source)
        except Exception:
            pass

    return HttpResponse()



def data_crawler(request):
    # total_uncrawled = Index.objects.all().filter(crawled=False).count() - 1
    # random_numbers_list = random.Random().sample(range(0, total_uncrawled), 20)

    # indeces = [Index.objects.all().filter(crawled=False)[i] for i in random_numbers_list]
    indeces = Index.objects.all().filter(crawled=False).order_by('?')[:30]

    for index in indeces:
        try:
            page = requests.get(index.link)
            tree = html.fromstring(page.text)

            job = Job()

            if tree.xpath('//title/text()'):
                meta_title = tree.xpath('//title/text()')[0].encode('utf-8')
                job.meta_title = meta_title[0:200]

            if tree.xpath('//meta[@name="description"]/@content'):
                meta_description = tree.xpath('//meta[@name="description"]/@content')[0].encode('utf-8')
                if not meta_description:
                    if tree.xpath('//meta[@name="og:description"]/@content'):
                        meta_description = tree.xpath('//meta[@name="og:description"]/@content')[0].encode('utf-8')
                job.meta_description = meta_description[0:200]

            source = index.source

            if source.title_xpath:
                if tree.xpath(source.title_xpath):
                    title = tree.xpath(source.title_xpath)[0].encode('utf-8').lower()
                    job.title = title[0:200]

        # if source.description_xpath:
        #     if tree.xpath(source.description_xpath):
        #         job.description = str(tree.xpath(source.description_xpath)[0]).lower()

            if index.link:
                job.landing_page = index.link

            if source.company_xpath:
                if tree.xpath(source.company_xpath):
                    job.company = Company.get_company_with_name(tree.xpath(source.company_xpath)[0].encode('utf-8').lower())

            if source.job_type_xpath:
                if tree.xpath(source.job_type_xpath):
                    jtype = str(tree.xpath(source.job_type_xpath)[0].encode('utf-8')).lower()
                    job.type = jtype[:20]

            extractor = Extractor(extractor='ArticleExtractor', url=index.link)
            job.content = extractor.getText()
            job.description = extractor.getText()

        # if source.salary_xpath:
        #     if tree.xpath(source.salary_xpath):
        #         job.salary = get_formated_salary(tree.xpath(source.salary_xpath)[0])

            if source.location_xpath:
                if tree.xpath(source.location_xpath):
                    _location = tree.xpath(source.location_xpath)[0].encode('utf-8').lower()
                    job.location = _location[:50]

            if source.qualifications_xpath:
                if tree.xpath(source.qualifications_xpath):
                    _qualification = tree.xpath(source.qualifications_xpath)[0].encode('utf-8')
                    job.qualification = _qualification[:100]

            job.source = index.source.name

            job.save()
            index.crawled = True
            index.save()

            if source.industry_xpath:
                if tree.xpath(source.industry_xpath):
                    tags = split_and_get_formatted_tags(tree.xpath(source.industry_xpath)[0].encode('utf-8'))

                    for tag in tags:
                        t = Tag.get_tag_with_name(tag[:200])
                        job.tags.add(t)

        except URLError, ex:
            # return HttpResponse('exception: '+ex.message+":"+index.link)
            index.delete()
            pass
        except ConnectionError, ex:
            # return HttpResponse('exception: '+ex.message+":"+index.link)
            index.delete()
            pass
        except OperationalError, ex:
            index.delete()
            pass
        except UnicodeEncodeError, ex:
            index.delete()
            pass
        except LocationParseError, ex:
            index.delete()
        except XMLSyntaxError, ex:
            index.delete()
        # except Exception:
        #     # silinced location parser error
        #     index.delete()
        #     pass





    return HttpResponse()




    pass

