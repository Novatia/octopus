from django.db import models


class Source(models.Model):
    name = models.CharField(max_length=200)
    start_url = models.CharField(max_length=500)
    base_path = models.CharField(max_length=500)
    link_xpath = models.CharField(max_length=500)
    company_xpath = models.CharField(max_length=500, blank=True, default='')
    job_type_xpath = models.CharField(max_length=500, blank=True, default='')
    industry_xpath = models.CharField(max_length=500, blank=True, default='')
    title_xpath = models.CharField(max_length=500, blank=True, default='')
    description_xpath = models.CharField(max_length=500, blank=True, default='')
    salary_xpath = models.CharField(max_length=500, blank=True, default='')
    summary_xpath = models.CharField(max_length=500, blank=True, default='')
    qualifications_xpath = models.CharField(max_length=500, blank=True, default='')
    location_xpath = models.CharField(max_length=500, blank=True, default='')
    content_xpath = models.CharField(max_length=200, blank=True, default='')
    years_of_experience_xpath = models.CharField(max_length=200, blank=True, default='')




    def __unicode__(self):
        return self.name


class Index(models.Model):
    link = models.URLField()
    crawled = models.BooleanField()
    source = models.ForeignKey(Source)

    def __unicode__(self):
        return self.link

    @classmethod
    def add_to_index(cls, links_lists, source):
        for link in links_lists:
            try:
                cls.objects.get(link=link)
            except:
                cls.objects.create(link=link, crawled=False, source=source)
                pass