from django.test import TestCase
from django.test.client import Client

from models import Source, Index
from helpers import split_and_get_formatted_tags

# Create your tests here.
class CrawlerTestCase(TestCase):
    def setUp(self):
        Source.objects.create(name='jobberman', start_url='http://www.jobberman.com/jobs-in-nigeria', base_path='http://www.jobberman.com', link_xpath='//p[@class="job-title"]//a/@href',
                              company_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[3]', job_type_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[9]', industry_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[7]/a',
                              title_xpath='//*[@id="new-home-page"]/div[5]/p[1]', description_xpath='//*[@id="new-home-page"]/div[6]/div[1]/div[1]', salary_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[15]', summary_xpath='//*[@id="new-home-page"]/div[6]/div[1]/div[1]',
                              qualifications_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[11]/a', location_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[5]', content_xpath='//*[@id="new-home-page"]/div[6]/div[1]/div[1]',
                              years_of_experience_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[13]/a')

        Source.objects.create(name='wokerman', start_url='http://www.jobberman.com/jobs-in-nigeria', base_path='http://www.jobberman.com', link_xpath='//p[@class="job-title"]//a/@href',
                              company_xpath='', job_type_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[9]', industry_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[7]/a',
                              title_xpath='', description_xpath='//*[@id="new-home-page"]/div[6]/div[1]/div[1]', salary_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[15]', summary_xpath='//*[@id="new-home-page"]/div[6]/div[1]/div[1]',
                              qualifications_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[11]/a', location_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[5]', content_xpath='//*[@id="new-home-page"]/div[6]/div[1]/div[1]',
                              years_of_experience_xpath='//*[@id="new-home-page"]/div[6]/div[2]/div[1]/p[13]/a')

    def test_index_crawler_with_source_with_all_data(self):
        c = Client()
        response = c.get('/crawler/index_crawler')
        self.assertEqual(response.content, '')

    def test_data_crawler_with_source_with_all_data(self):
        c = Client()
        response = c.get('/crawler/data_crawler')
        self.assertEqual(response.content, '')

    def test_split_and_test_formated_tags_works(self):
        result = split_and_get_formatted_tags('Executive / Top Management and Consulting/Business Strategy & Planning')
        self.assertEqual(result, ['executive', 'top management', 'consulting', 'business strategy', 'planning'])

