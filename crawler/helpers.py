import re

def get_formated_salary(result):
    p = re.compile('[0-9]+')
    m = p.search(result)

    if m:
        amount = m.group()
        return amount

    return 0

def split_and_get_formatted_tags(extracted_tag):
    results = []


    temp = extracted_tag.split('/')

    for i in temp:
        if '&' in i:
            val = i
            # results.remove(i)
            temp_ = val.split('&')
            for i in temp_:
                results.append(i.strip())
        elif 'and' in i:
            val = i
            temp_ = val.split('and')
            for i in temp_:
                results.append(i.strip())
        else:
            results.append(i.strip())

    return [i.lower() for i in results]




