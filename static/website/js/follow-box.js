//Insert this code to the head section
$(document).ready(function () {

    $('.share-popup').animate({
        'bottom': 0,
    }, 'fast');

    $('.share-btn-close').click(function () {
        $('.share-popup').animate({
            'bottom': -400,
        }, 'slow');
    });

});
//End of Code.