$(function(){
    $('#subscribe_button').on('click', function(event){
        event.preventDefault();

        var email = $('#top_email_alert_email').val();
        var q = $('input[name="q"]').val();

        if(!email){
           alert('You need to enter your email');

            return false;
        }

        var box = $('#email-alert-box');
        var error_box = $('#error_span');
        var subscribe_path = '/mailing/subscribe/' + '?email='+email+'&q='+q;

        var success_message = '<span style="display: block; margin-bottom: 5px;">Your email alert has been created and a welcome message has been sent to you.</span>';
        var error_message = '<span style="display: block; margin-bottom: 5px;" style="color:red">Email address has already been saved in our database.</span>';


        $.get(subscribe_path, function(data){
            if(data == 'true'){
                box.html(success_message);
            }
            else{
                error_box.replaceWith(error_message);
            }
        })



        return false;
    });

})