$(function(){
    $('#subscribe_button').on('click', function(event){
        event.preventDefault();

        var email = $('#top_email_alert_email').val();
        if(!email){
           alert('You need to enter your email');

            return false;
        }



        var box = $('#job_alert_top');
        var error_box = $('#error_span');
        var subscribe_path = '/mailing/subscribe/' + '?email='+email;

        var success_message = '<span class="jal">Your email alert has been created and a welcome message has been sent to you.</span>';
        var error_message = '<span class="jal" style="color:red">Email address has already been saved in our database.</span>';


        $.get(subscribe_path, function(data){
            if(data == 'true'){
                box.html(success_message);
            }
            else{
                error_box.replaceWith(error_message);
            }
        })



        return false;
    });

})